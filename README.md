[![pipeline status](https://gitlab.com/ocappelle/reactive-movie/badges/master/pipeline.svg)](https://gitlab.com/ocappelle/reactive-movie/commits/master)

[![coverage report](https://gitlab.com/ocappelle/reactive-movie/badges/master/coverage.svg)](https://gitlab.com/ocappelle/reactive-movie/commits/master)

# reactive-movie

mvn sonar:sonar \
  -Dsonar.host.url=http://localhost:9000 \
  -Dsonar.login=072c2f091deef09e14dc25866e1a7798f8b4a46f
  
# swagger

See swagger results at 
* `http://localhost:8085/swagger-ui.html`
* `http://localhost:8085/v2/api-docs`
* `http://localhost:8085/swagger-resources`

# Ui

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
  