package com.realdolmen.reactivemovie.bootstrap;

import com.realdolmen.reactivemovie.model.Movie;
import com.realdolmen.reactivemovie.repository.MovieRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.time.LocalDate;

@Profile({"local", "test"})
@Component
@AllArgsConstructor
public class BootstrapDataListener implements /*CommandLineRunner,*/ ApplicationListener<ContextRefreshedEvent> {
    private final MovieRepository movieRepository;

    /*@Override
    public void run(String... args) throws Exception {
        if (movieRepository.count().block() == 0) {
            movieRepository.saveAll(getMovies().collectList().block());
        }
    }*/

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        /*getMovies().subscribe(
                movie -> movieRepository.save(movie).block()
        );*/
        if (movieRepository.count().block() == 0) {
            movieRepository.saveAll(getMovies().collectList().block()).collectList().block();
        }
    }

    private Flux<Movie> getMovies() {
        return Flux.just(
                Movie.builder().id(90000L).title("Dangal").description("India wrestling").releaseDate(LocalDate.of(2016, 1, 1)).build(),
                Movie.builder().id(90001L).title("Thor").description("Marvel superhero").releaseDate(LocalDate.of(2014, 1, 1)).build(),
                Movie.builder().id(90002L).title("Les Intouchables").description("Film fr").releaseDate(LocalDate.of(2015, 1, 1)).build(),
                Movie.builder().id(90003L).title("Fantastic Beasts 2").description("Harry Potter sequel").releaseDate(LocalDate.of(2018, 1, 1)).build()
        );
    }

}
