package com.realdolmen.reactivemovie.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
public class DirectorDto extends PersonDto {
    private List<MovieDto> movies;

    public DirectorDto(String firstName, String lastName, List<MovieDto> movies) {
        super(firstName, lastName);
        this.movies = movies;
    }
}
