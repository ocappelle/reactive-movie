package com.realdolmen.reactivemovie.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@Document
public class Movie extends BaseEntity {
    @NonNull
    @NotBlank
    @ApiModelProperty(value = "Title of the movie", required = true)
    private String title;

    @ApiModelProperty(value = "Description of the movie")
    private String description;

    @ApiModelProperty(value = "Release date of the movie")
    private LocalDate releaseDate;

    @DBRef
    @ApiModelProperty(value = "Director of the movie")
    private Director director;

    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    public Movie(Long id, @NonNull String title, String description,
                 LocalDate releaseDate, Director director) {
        super(id);
        this.title = title;
        this.description = description;
        this.releaseDate = releaseDate;
        this.director = director;
    }
}
