package com.realdolmen.reactivemovie.config;

import com.realdolmen.reactivemovie.bootstrap.BootstrapDataListener;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;

@TestConfiguration
@ComponentScan(basePackageClasses = BootstrapDataListener.class)
public class BootstrapConfig {
}
