package com.realdolmen.reactivemovie.controller;

import com.realdolmen.reactivemovie.model.Movie;
import com.realdolmen.reactivemovie.service.MovieService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MovieControllerTest {
    @Mock
    private MovieService service;

    @InjectMocks
    private MovieController controller;

    private WebTestClient webTestClient;

    @Before
    public void setUp() {
        webTestClient = WebTestClient
                .bindToController(controller)
                .build();
    }

    @Test
    public void getAll() {
        Movie[] expectedResult = {
                Movie.builder().title("Test").build(),
                Movie.builder().title("Test 2").build(),
                Movie.builder().title("Test 3").build()
        };

        given(service.findAll())
                .willReturn(Flux.just(expectedResult));

        webTestClient.get()
                .uri("/movie")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(Movie.class)
                .hasSize(expectedResult.length)
                .contains(expectedResult);

        verify(service, times(1)).findAll();
    }
}