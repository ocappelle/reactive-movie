package com.realdolmen.reactivemovie.model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static com.realdolmen.reactivemovie.model.MovieTestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class MovieTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testBuilderSuccess() {
        Movie movie = Movie.builder()
                .title("avengers")
                .build();

        assertNotNull(movie);
    }

    @Test
    public void testBuilderMissingTitle() {
        expectedException.expect(NullPointerException.class);

        Movie.builder().description("description").build();
    }

    @Test
    public void testEqualsMethod() {
        Flux<Movie> movieFlux = Flux.just(minimalBuilder().build(), minimalBuilderAlternative().build());

        StepVerifier.create(movieFlux)
                .expectNextMatches(movie -> movie.equals(Movie.builder().title(TITLE).build()))
                .consumeNextWith(movie -> assertThat(movie, is(Movie.builder().title(ALTERNATIVE_TITLE).build())))
                .expectComplete()
                .verify();
    }
}