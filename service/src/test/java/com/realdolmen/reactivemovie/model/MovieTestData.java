package com.realdolmen.reactivemovie.model;

public class MovieTestData {

    public static final String TITLE = "Avengers";
    public static final Long ID = 1L;
    public static final String ALTERNATIVE_TITLE = "Harry Potter";

    public static Movie.Builder minimalBuilder() {
        return Movie.builder()
                .title(TITLE);
    }

    public static Movie.Builder minimalBuilderAlternative() {
        return Movie.builder()
                .title(ALTERNATIVE_TITLE);
    }

    public static Movie.Builder minimalBuilderWithId() {
        return Movie.builder()
                .id(ID)
                .title(TITLE);
    }
}
