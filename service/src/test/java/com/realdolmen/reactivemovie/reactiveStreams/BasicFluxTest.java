package com.realdolmen.reactivemovie.reactiveStreams;

import com.realdolmen.reactivemovie.model.Movie;
import org.junit.Ignore;
import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;

public class BasicFluxTest {

    @Test
    public void testEmptyFlux() {
        Flux<String> flux = Flux.empty();

        StepVerifier.create(flux)
                .verifyComplete();
    }

    @Test
    public void testListOfStrings() {
        Flux<String> flux = Flux.just("foo", "bar");

        StepVerifier.create(flux)
                .expectNext("foo", "bar")
                .expectComplete()
                .verify();
    }

    @Test
    public void testThrowIllegalStateException() {
        Flux<String> flux = Flux.error(IllegalStateException::new);

        StepVerifier.create(flux)
                .expectError(IllegalStateException.class)
                .verify();
    }

    @Test
    public void countEach1ms() {
        Flux<Long> flux = Flux
                .interval(Duration.ofMillis(1))
                .take(10);

        StepVerifier.withVirtualTime(() -> flux)
                .expectNext(0L, 1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L)
                .expectComplete()
                .verify();
    }

    @Test
    @Ignore //TODO fix test
    public void countWithVirtualTime() {
        Flux<Long> flux = Flux
                .interval(Duration.ofMinutes(1))
                .take(60);

        StepVerifier.withVirtualTime(() -> flux)
                .thenAwait(Duration.ofHours(1))
                .expectNextCount(60)
                .expectComplete()
                .verify();
    }

    @Test
    public void test2elementsThenError() {
        Flux<String> flux = Flux.just("foo", "bar").concatWith(Mono.error(RuntimeException::new));

        StepVerifier.create(flux)
                .expectNext("foo", "bar")
                .expectError(RuntimeException.class)
                .verify();
    }

    @Test
    public void testSynchronousCapitalizing() {
        String title = "title";
        String description = "description";

        Mono<Movie> movieMono = Mono.just(Movie.builder().title(title).description(description).build());

        Mono<Movie> capitalizeSync = movieMono.map(
                movie -> movie.toBuilder()
                        .title(movie.getTitle().toUpperCase())
                        .description(movie.getDescription().toUpperCase())
                        .build()
        );

        StepVerifier.create(capitalizeSync)
                .expectNext(Movie.builder().title(title.toUpperCase()).description(description.toUpperCase()).build())
                .expectComplete()
                .verify();
    }

    @Test
    public void testAsynchronousCapitalizing() {
        String title = "title";
        String title2 = "title2";
        String description = "description";
        String description2 = "description2";

        Movie movie1 = Movie.builder().title(title).description(description).build();
        Movie movie2 = Movie.builder().title(title2).description(description2).build();

        Flux<Movie> movieFlux = Flux.just(movie1, movie2);

        Flux<Movie> capitalizeAsync = movieFlux.flatMap(
                movie -> Mono.just(movie.toBuilder()
                        .title(movie.getTitle().toUpperCase())
                        .description(movie.getDescription().toUpperCase())
                        .build())
        );

        StepVerifier.create(capitalizeAsync)
                .expectNext(Movie.builder().title(title.toUpperCase()).description(description.toUpperCase()).build())
                .expectNext(Movie.builder().title(title2.toUpperCase()).description(description2.toUpperCase()).build())
                .expectComplete()
                .verify();
    }

    @Test
    public void testLogFunctionality() {
        Flux<Movie> movieFlux = Flux.just(
                Movie.builder().title("test1").build(),
                Movie.builder().title("test2").build(),
                Movie.builder().title("test3").build(),
                Movie.builder().title("test4").build()
        );

        Flux<Movie> fluxWithLog = movieFlux.log();

        StepVerifier.create(fluxWithLog, 0)
                .thenRequest(1)
                .expectNextMatches(u -> true)
                .thenRequest(1)
                .expectNextMatches(u -> true)
                .thenRequest(2)
                .expectNextMatches(u -> true)
                .expectNextMatches(u -> true)
                .expectComplete()
                .verify();
    }

    @Test
    public void testDoOnFunctionality() {
        Flux<Movie> movieFlux = Flux.just(
                Movie.builder().title("test1").build(),
                Movie.builder().title("test2").build(),
                Movie.builder().title("test3").build(),
                Movie.builder().title("test4").build()
        );

        Flux<Movie> fluxWithLog = movieFlux
                .doOnSubscribe(s -> System.out.println("Subscribing:"))
                .doOnRequest(r -> System.out.println("Requesting:"))
                .doOnEach(e -> System.out.println("signal"))
                .doOnNext(movie -> System.out.println(movie.getTitle()))
                .doOnCancel(() -> System.out.println("Cancel.."))
                .doOnTerminate(() -> System.out.println("Terminating.."));


        StepVerifier.create(fluxWithLog, 0)
                .thenRequest(1)
                .expectNextMatches(u -> true)
                .thenRequest(1)
                .expectNextMatches(u -> true)
                .thenRequest(2)
                .expectNextMatches(u -> true)
                .expectNextMatches(u -> true)
                .expectComplete()
                .verify();
    }

    @Test
    public void testReturnValueInsteadOfException() {
        Mono<String> mono = Mono.error(IllegalStateException::new);

        StepVerifier.create(handleError(mono))
                .expectNext("default")
                .expectComplete()
                .verify();

        Mono<String> mono2 = Mono.just("test");

        StepVerifier.create(handleError(mono2))
                .expectNext("test")
                .expectComplete()
                .verify();
    }

    private Mono<String> handleError(Mono<String> mono) {
        return mono.onErrorReturn("default");
    }

    @Test
    public void testReturnFluxValueInsteadOfException() {
        Flux<String> flux = Flux.error(IllegalStateException::new);

        StepVerifier.create(handleError(flux))
                .expectNext("fallbackValue", "otherFallbackValue")
                .expectComplete()
                .verify();

        Flux<String> flux2 = Flux.just("test", "test2");

        StepVerifier.create(handleError(flux2))
                .expectNext("test", "test2")
                .expectComplete()
                .verify();
    }

    private Flux<String> handleError(Flux<String> flux) {
        return flux.onErrorResume(e -> Flux.just("fallbackValue", "otherFallbackValue"));
    }

    @Test
    public void testRequestLimitAmount() {
        Flux<Integer> flux = Flux.just(1, 2, 3, 4);

        StepVerifier.create(flux, 1)
                .expectNext(1)
                .thenRequest(1)
                .expectNext(2)
                .thenCancel();
    }
}
