package com.realdolmen.reactivemovie.repository;

import com.realdolmen.reactivemovie.bootstrap.BootstrapDataListener;
import com.realdolmen.reactivemovie.config.BootstrapConfig;
import com.realdolmen.reactivemovie.model.Movie;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataMongoTest
@ActiveProfiles("test")
@Import(BootstrapConfig.class)
public class MovieRepositoryServiceTest {
    @Autowired
    private MovieRepository repository;

    @Autowired
    private BootstrapDataListener bootstrapDataListener;

    @Before
    public void init() {
        repository.deleteAll().block();

        bootstrapDataListener.onApplicationEvent(null);
    }

    @Test
    public void findByTitle() {
        Flux<Movie> actualResult = repository.findByTitle("Thor");

        assertThat(actualResult.count().block(), is(1L));
    }
}