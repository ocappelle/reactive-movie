package com.realdolmen.reactivemovie.route.config;

import com.realdolmen.reactivemovie.AbstractServiceTest;
import com.realdolmen.reactivemovie.model.Movie;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import reactor.core.publisher.Flux;

import static com.realdolmen.reactivemovie.model.MovieTestData.*;
import static com.realdolmen.reactivemovie.route.config.MovieRouter.REACTIVE_MOVIE_ALTERNATIVE_ENDPOINT;
import static com.realdolmen.reactivemovie.route.config.MovieRouter.REACTIVE_MOVIE_ENDPOINT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;

public class MovieRouterTest extends AbstractServiceTest {

    private Movie movie1;
    private Movie movie2;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        movie1 = minimalBuilderWithId().build();
        movie2 = minimalBuilderAlternative().id(2L).build();
        movieRepository.saveAll(Flux.just(movie1, movie2)).collectList().block();
    }

    @Test
    public void testFindAll() {
        webTestClient.get().uri(REACTIVE_MOVIE_ENDPOINT)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Movie.class)
                .hasSize(2)
                .contains(movie1, movie2);
    }

    @Test
    public void testFindById() {
        webTestClient.get()
                .uri(REACTIVE_MOVIE_ENDPOINT + "/1")
                .exchange()
                .expectStatus().isOk()
                .expectBody(Movie.class)
                .isEqualTo(movie1);
    }


    @Test
    public void testFindAllAlternative() {
        webTestClient.get().uri(REACTIVE_MOVIE_ALTERNATIVE_ENDPOINT)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Movie.class)
                .hasSize(2)
                .contains(movie1, movie2);
    }

    @Test
    public void testFindByIdAlternative() {
        webTestClient.get()
                .uri(REACTIVE_MOVIE_ALTERNATIVE_ENDPOINT + "/1")
                .exchange()
                .expectStatus().isOk()
                .expectBody(Movie.class)
                .isEqualTo(movie1);
    }

    @Test
    public void testFindByIdVariationAlternative() {
        webTestClient.get()
                .uri(REACTIVE_MOVIE_ALTERNATIVE_ENDPOINT + "/variation/1")
                .exchange()
                .expectStatus().isOk()
                .expectBody(Movie.class)
                .isEqualTo(movie1);
    }

    @Test
    public void testFindByTitleAlternative() {
        webTestClient.get()
                .uri(REACTIVE_MOVIE_ALTERNATIVE_ENDPOINT + "/title/" + TITLE)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Movie.class)
                .contains(movie1);
    }

    @Test
    @Ignore //TODO fix this test
    public void testCreateAlternative() {
        Movie venom = Movie.builder().title("Venom").build();
        Movie movieAfterSave = webTestClient.post()
                .uri(REACTIVE_MOVIE_ALTERNATIVE_ENDPOINT)
                .body(fromObject(venom))
                .exchange()
                .expectStatus().isCreated()
                .expectBody(Movie.class)
                .returnResult()
                .getResponseBody();

        assertNotNull(movieAfterSave);
        assertThat(movieAfterSave.getTitle(), is(venom.getTitle()));
    }
}