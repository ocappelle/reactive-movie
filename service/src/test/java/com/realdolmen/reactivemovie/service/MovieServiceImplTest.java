package com.realdolmen.reactivemovie.service;

import com.realdolmen.reactivemovie.model.Movie;
import com.realdolmen.reactivemovie.repository.MovieRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.List;

import static com.realdolmen.reactivemovie.model.MovieTestData.*;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MovieServiceImplTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private MovieRepository repository;

    @InjectMocks
    private MovieServiceImpl service;

    @Before
    public void setUp() {
    }

    @Test
    public void findByTitle() {
        Movie expectedMovie = minimalBuilder().build();

        given(repository.findByTitle(TITLE))
                .willReturn(Flux.just(expectedMovie));

        List<Movie> actualMovies = service.findByTitle(TITLE).collectList().block();

        assertThat(actualMovies.size(), is(1));
        assertThat(actualMovies, containsInAnyOrder(expectedMovie));

        verify(repository, times(1)).findByTitle(TITLE);
    }

    @Test
    public void findById() {
        Movie expectedMovie = minimalBuilderWithId().build();
        Mono<Long> idMono = Mono.just(ID);

        given(repository.findById(idMono))
                .willReturn(Mono.just(expectedMovie));

        Movie actualMovie = service.findById(idMono).block();

        assertThat(actualMovie, is(expectedMovie));

        verify(repository, times(1)).findById(idMono);
    }

    @Test
    public void findAll() {
        Movie expectedMovie1 = minimalBuilder().build();
        Movie expectedMovie2 = minimalBuilderAlternative().build();

        given(repository.findAll())
                .willReturn(Flux.just(expectedMovie1, expectedMovie2));

        Flux<Movie> actualMovieFlux = service.findAll();

        StepVerifier.create(actualMovieFlux)
                .expectNext(expectedMovie1)
                .expectNext(expectedMovie2)
                .expectComplete()
                .verify();

        List<Movie> actualMovies = actualMovieFlux.collectList().block();

        assertThat(actualMovies, containsInAnyOrder(expectedMovie1, expectedMovie2));

        verify(repository, times(1)).findAll();
    }

    @Test
    public void update() {
    }

    @Test
    public void create() {
    }

    @Test
    public void createWithPublisher() {
    }
}